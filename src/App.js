import React from "react";
import "./App.css";
import ListItem from "./components/ListItem";
import ClearBtn from "./components/ClearBtn";
// import DataContext from "./contexts/DataContext";

function App() {
  // const contextValue = {
  //   items: [items]
  // }
  return (
    // <DataContext.Provider value={contextValue}>
    <div className="App">
      <header className="App-header">
        <h1>My Todo List</h1>
      </header>
      <ListItem />
      <ClearBtn />
    </div>
    // </DataContext.Provider>
  );
}

export default App;
