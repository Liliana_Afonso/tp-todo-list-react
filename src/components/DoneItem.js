import React from "react";
import "../assets/scss/button.scss";
import "../assets/scss/form.scss";
import "../assets/scss/list.scss";
import "../App.css";

const DoneItem = () => {
    return (
        <li className="doneItem">
            <button className="btn btn--green">
                <img src={require("../assets/img/correct.svg")} alt="" />
            </button>
        </li >
    )
};

export default DoneItem;