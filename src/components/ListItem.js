import React, { useState, useEffect } from "react";
import AddItem from "./AddItem";
import "../assets/scss/button.scss";
import "../assets/scss/form.scss";
import "../App.css";


const ListItem = () => {

  const KEY = "input";
  const [items, setItems] = useState([]);
  const [input, setInput] = useState("");

  React.useEffect(() => {
    const localData = localStorage.getItem(KEY);
    if (localData) {
      setItems(JSON.parse(localData));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem(KEY, JSON.stringify(items))
  });


  const handleSubmit = (e, items, setItems, input, setInput) => {
    console.log("length " + items.length + " key " + KEY)
    console.log(input);
    e.preventDefault();
    setItems([...items, { txt: input }]);
    setInput("");

  };

  const deleteItem = (txt, items, setItems) => {
    setItems(items.filter(item => item.txt !== txt));
  };

  return (
    <div className="listItem">
      <form id="form" onSubmit={(e) => handleSubmit(e, items, setItems, input, setInput)}>
        <input className="input" placeholder="new todo" onChange={(e) => setInput(e.target.value)} value={input}></input>
        <button className="btn btn--blue btn-add">
          <img src={require("../assets/img/plus.svg")} alt="" />
        </button>
      </form>
      {items.map(item => (
        <AddItem
          // key={KEY}
          txt={item.txt}
          deleteItem={(txt) => deleteItem(txt, items, setItems)}
        />
      ))}
    </div>
  );
};

export default ListItem;
