import React from "react";
import "../assets/scss/button.scss";
import "../App.css";

const ClearBtn = (setItems) => {
    const clearItems = () => {
        localStorage.clear()
        setItems("");
    };

    return (
        <div className="clearBtn">
            <button className="btn btn--red btn--clear" onClick={() => clearItems()} > cleat all</button>
        </div>
    )
}
export default ClearBtn;