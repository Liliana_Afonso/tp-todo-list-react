import React, { useState, useEffect } from "react";
import "../assets/scss/button.scss";
import "../assets/scss/form.scss";
import "../assets/scss/list.scss";
import "../App.css";
import DoneItem from "./DoneItem";

const AddItem = ({ txt, deleteItem }) => {
  const KEY = "check";

  const [check, setCheck] = useState(false);

  React.useEffect(() => {
    localStorage.getItem(KEY);
  }, []);

  useEffect(() => {
    localStorage.setItem(KEY, check)
  });

  return (
    <div className="addItem">
      <ul className="list">
        {check ? (<DoneItem />) : ("")}
        {check ?
          (<li className="item" onClick={() => setCheck(false)}>{txt}</li>) :
          (<li className="item" onClick={() => setCheck(true)}>{txt}</li>)
        }

        <li className="delete-btn"><button className="btn btn--red" onClick={() => deleteItem(txt)}>
          <img src={require("../assets/img/close.svg")} alt="" />
        </button>
        </li>
      </ul>

    </div >
  );
};

export default AddItem;
